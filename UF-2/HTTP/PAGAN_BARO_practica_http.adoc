= Pràctica servidor HTTP

:encoding: utf-8
:lang: ca
:toc: left
:!numbered:


== Objectiu de la pràctica

L'objectiu de la pràctica és configurar un servidor Apache i veure'n les
característiques més comunes.

== Preparatius

* Utilitzarem l'entorn de xarxa de les pràctiques anteriors. Configurarem
_tyr_ (172.16.100.2) com a servidor HTTP.

* Qualsevol màquina podrà actuar com a client del HTTP. Si tenim el servei DNS
funcionant, podrem accedir-hi directament pel nom del servidor, en cas contrari
ho haurem de fer utilitzant la seva IP.

* Si no tenim el servei DHCP funcionant, haurem d'assignar una IP fixa a PC1
per tal que es pugui connectar a la xarxa.

* Seguint els passos de les pràctiques anteriors, totes les màquines han de
mostrar un prompt personalitzat i hi ha d'haver un usuari amb el vostre nom i
permisos per utilitzar *sudo*.

[IMPORTANT]
====
Cal que configureu el vostre terminal perquè al prompt aparegui el vostre nom i
la data ressaltats en diferents colors. *Això és imprescindible per avaluar
les captures*.
====

== Apartat 1: Instal·lació de l'Apache a *tyr*

La màquina *tyr* ja s'ha creat a la pràctica de FTP. En cas de no tenir-la,
cal clonar-la de *base* i assignar-li el seu nom i configuració de xarxa.

Instal·la el paquet *apache2* a *tyr* i comprova que ja s'hi pot accedir
des de la xarxa local. Per a comprovar que la connexió funciona des d'un
ordinador sense entorn gràfic, pots utilitzar el navegador en línia de
comandes *lynx* o *w3m*.

.Entregar
====
image::images/e1.PNG[]
====
====
*Instal·lació de l'apache2*
====

.Entregar
====
image::images/e2.PNG[]
====
====
*Comprovació de connexió des de la xarxa local*
====

Finalment, per poder accedir des de l'exterior de la xarxa caldrà obrir i
readreçar correctament els ports 80 i 443 a *heimdall*. Per fer això utilitzarem
l'*iptables* igual que ja hem fet a les pràctiques anteriors.

.Entregar
====
image::images/e3.PNG[]
====
====
*Ordres de l'_iptables_ per dirigir els ports 80 i 443 cap a _tyr_*
====

Un cop fet això ja pots provar la connexió a la web per defecte des la xarxa
del centre, utilitzant qualsevol navegador web.

.Entregar
====
image::images/e4.PNG[]
====
====
*Comprovació de connexió des de la xarxa del centre*
====

== Apartat 2: Creació de hosts virtuals

En aquest apartat farem que el nostre servidor respongui a dos noms de hosts
diferents. Per una banda, tindrem el nom per defecte, *nom.cognom.local*, el
mateix que vam utilitzar a la pràctica dns. A aquest afegirem el host
*cognom.nom.local*, que es dirigirà al mateix servidor, però a una pàgina web
diferent.

Per aconseguir això caldrà fer dues coses:

1. Configurar el servidor DNS per tal que les peticions als dos dominis es
dirigeixin correctament a la IP pública de *heimdall*.
2. Configurar l'Apache per tal que distingeixi els dos noms i mostri la pàgina
correcta en cada cas.

La primera part s'ha d'afegir al servidor DNS de l'entorn de pràctiques, requerint
 la creació de la zona i dels registres necessaris al bind9 de *odin*.

.Entregar
====
image::images/e5.PNG[]
====
====
*Captura del fitxer _/etc/bind/named.conf.local_.*
====
endif::teacher[]
ifdef::teacher[]
.Solució
endif::teacher[]

ifndef::teacher[]
.Entregar
====
image::images/e6.PNG[]
====
====
*Captura del fitxer de la zona.*
====
endif::teacher[]
ifdef::teacher[]
.Solució
endif::teacher[]

ifndef::teacher[]
.Entregar
====
image::images/e7.PNG[]
====
====
*Comprovació que la nova zona funciona correctament.*
====
endif::teacher[]
ifdef::teacher[]
.Solució
endif::teacher[]

Anem a crear ara les pàgines web de cada domini.

- Crea l'estructura de directoris necessària. La web del domini *nom.cognom.local*
es llegirà des del directori */var/www/nom*, mentre que la web del domini
*cognom.nom.local* es llegirà del directori */var/www/cognom*.

- Crea dos fitxers *index.html*, un a cadascun dels directoris anteriors. Escriu
un codi HTML a cadascun d'ells de manera que es vegi clarament a quin dels
hosts virtuals hem connectat en cada cas.

.Entregar
====
image::images/e8.PNG[]
====
====
*Sortida de `ls -R` on es vegi l'estructura de fitxers i directoris de _/var/www_.*
====

.Entregar
====
image::images/e9.PNG[]
====
====
image::images/e9a.PNG[]
====
====
*Cadascun dels dos fitxers _index.html_.*
====

- Crea fitxers de configuració de l'Apache, un per cada host virtual. Utilitza
com a model el fitxer */etc/apache2/sites-available/000-default.conf*. No
t'oblidis de configurar les directives *ServerName* i *DocumentRoot*.

.Entregar
====
image::images/e10.PNG[]
====
====
image::images/e10a.PNG[]
====
====
*Cadascun dels dos fitxers de configuració creats.*
====

- Activa els dos hosts virtuals que hem creat i desactiva el host per defecte.

.Entregar
====
image::images/e11.PNG[]
====
====
*Instruccions utilitzades per activar i desactivar els hosts virtuals.*
====

.Entregar
====
image::images/e12.PNG[]
====
====
*Sortida de l'ordre `ls` on es vegin els sites actius a _sites-enabled_.*
====

- Reinicia l'Apache utilitzant l'ordre *apachectl*.

- Comprova que pots accedir a cadascun dels hosts des del teu ordinador.

.Entregar
====
image::images/e13.PNG[]
====
====
image::images/e13a.PNG[]
====
====
*Captura del navegador connectat a cadascun dels dos sites.*
====

== Apartat 3: Configuració del primer host virtual

Les següents configuracions s'han d'aplicar al site *nom.cognom.local*. Recorda
que sempre cal reinciar l'Apache per tal que agafi els canvis.

- Canvia el nom del fitxer *index.html* a *inici.html*. Fes que se serveixi
aquest fitxer per defecte en comptes de *index.html*.

.Entregar
====
image::images/e14.PNG[]
====
====
image::images/e14a.PNG[]
====
====
*Canvis fets a la configuració.*
====

- Utilitza la directiva *ServerAlias* per fer que si l'usuari utilitza el nom
*www.nom.cognom.local* en comptes de *nom.cognom.local*, la pàgina es mostri
correctament.

Per tal que això funciona, el servidor DNS ha de resoldre correctament l'adreça
*www.nom.cognom.local* a la teva adreça IP.

.Entregar
====
image::images/e15.PNG[]
====
====
*Canvis fets a la configuració.*
====

.Entregar
====
image::images/e16.PNG[]
====
====
*Comprovació que funciona.*
====

- Crea un tercer directori dins de */var/www* anomenat *test* i posa un
fitxer *inici.html* a dins que mostri clarament quin lloc estem visitant.
Després, utilitza la directiva *Alias* per fer que l'adreça
*nom.cognom.local/test* vagi a parar a aquest directori.

.Entregar
====
image::images/e17.PNG[]
====
====
image::images/e17a.PNG[]
====
====
*Canvis fets a la configuració.*
====

.Entregar
====
image::images/e18.PNG[]
====
====
*Comprovació que funciona.*
====

- Configura l'Apache per tal que permiti l'ús de fitxers *.htaccess* al
directori */var/www/test*, però assegura't que no en permet als altres
directoris.

.Entregar
====
image::images/e19.PNG[]
====
====
*Canvis fets a la configuració.*
====

- Utilitza un fitxer *.htaccess* per fer que per accedir al directori
*/var/www/test* s'hagi de proporcionar un nom d'usuari i contrasenya vàlids.
Hi haurà dos usuaris amb permís d'accés: *nom* i *cognom*, els
dos amb contrasenya *12345*.

.Entregar
====
image::images/e20.PNG[]
====
====
*Canvis fets a la configuració.*
====

.Entregar
====
image::images/e21.PNG[]
====
====
image::images/e21a.PNG[]
====
====
*Comprovació que funciona.*
====

== Apartat 4: Configuració del segon host virtual

Les següents configuracions s'han d'aplicar al site *cognom.nom.local*.

Voldrem que aquest site se serveixi sempre per HTTPS en comptes d'HTTP. Per fer
això segueix els següents passos:

- Copia el fitxer */etc/apache2/sites-available/default-ssl.conf* per utilitzar
de model.

- Modifica la copia que has fet del fitxer amb les dades adequades pel teu
host. Pots utilitzar el mateix certificat SSL que vas utilitzar a la pràctica
del FTP o generar-ne un de nou.

.Entregar
====
image::images/e22.PNG[]
====
====
image::images/e22a.PNG[]
====

====
*Canvis fets a la configuració.*
====

- Un cop modificada la configuració, activa el nou site.

.Entregar
====
image::images/e23.PNG[]
====
====
*Instrucció utilitzada.*
====

- Comprova amb el navegador que podem accedir tant per HTTP com per HTTPS a
aquest site.

.Entregar
====
image::images/e24.PNG[]
====
====
image::images/e24a.PNG[]
====
====
*Comprovació que funciona.*
====

- Forçarem ara que qualsevol petició dirigida a HTTP es transformi
automàticament a HTTPS. Per això, afegeix les següent línies a la configuració
del site HTTP:

----
RewriteEngine on
RewriteCond %{SERVER_NAME} =cognom.nom.lan [OR]
RewriteCond %{SERVER_NAME} =*.cognom.nom.lan
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,QSA,R=permanent]
----

Amb això li estem demanant a l'Apache que reescrigui qualsevol petició dirigida
a *cognom.nom.local*, o qualsevol dels seus subdominis, a la mateixa adreça que
s'ha demanat, però utilitzant el protocol HTTPS.

- Comprova amb el navegador que si accedim ara per HTTP, la petició es
modifica automàticament.

.Entregar
====
image::images/e25.PNG[]
====
====
image::images/e25a.PNG[]
====
====
*Captura del fitxer de registre d'accés de l'Apache, on es veu la reescriptura
de l'adreça.*
====

== Apartat 5: Activació de pàgines personals pels usuaris

Activa les pàgines personals de cada usuari del sistema tal com s'explica als
apunts de teoria. Fes que el directori *public* dels usuaris es pugui utilitzar
per guardar la seva pàgina personal, que serà visible a l'adreça
*http://nom.cognom.local/~usuari/*.

Utilitza la següent regla de reescriptura per substituir el símbol ~ de les
adreces per _users_, de manera que per accedir a la pàgina personal de
l'usuari *nom* es pugui fer amb l'adreça *http://nom.cognom.local/users/nom*

----
RewriteEngine on
RewriteRule ^/users/(.*)$ /~$1 [R]
----

.Entregar
====
image::images/e26.PNG[]
====
====
image::images/e26a.PNG[]
====
====
*Comprovació que ens podem connectar a la carpeta d'un usuari.*
====

== Apartat 6: Instal·lació d'un LAMP

Segueix les instruccions dels apunts de teoria per aconseguir que el servidor
Apache tingui suport per PHP i puguin connectar-se a una base de dades.

.Entregar
====
image::images/e27.PNG[]
====
====
image::images/e27a.PNG[]
====
====
*Captura de l'accés a phpmyadmin.*
====

.Entregar
====
image::images/e28.PNG[]
====
====
image::images/e28a.PNG[]
====
====
*Captura de l'execució de l'script php de prova.*
====
